void resetSwitch(){
  
  viewerOffset = viewerOffsetVal;
  centreRestPos = viewerOffsetVal*2;
  
  headAge = 0;
  headAqr = 0;
  hairLength = 0;
  miscCountLeft = -roomX/2;
  miscCountRight = roomX/2;
  restCount = 0;
  resDown=false;
  boneCount = roomZ;
  activeCoins=1;
  for (int i=coinsTotal-1; i>=0; i--){
    cObjs.get(i).r=1;
  } 
  
  playHeadsAudio = true;
  playHairAudio = true;
  playMiscAudio = true;
  playRestAudio = true;
  playBonesAudio = true;
  playOrganicAudio = true;
  playCoinsAudio=true;
  
  snd.reset();
  
}

void resetLang(){
  
  if (french == false){
    french = true;
  } else {
    french = false;
  }
    
  changeLanguage=false;
  
  resetSwitch();
  
}
