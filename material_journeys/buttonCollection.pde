collButtonObject cbObj;
ArrayList<collButtonObject> cbObjs;
int collbuttonTotal;

class collButtonObject {
  int id;
  
  float posX;
  float posY;
  
  color myColour;
  
  boolean active = false;
  
  collButtonObject(int index){
    id = index;
    if (id==0){myColour = color(0, 255, 0);}
    if (id==1){myColour = color(0, 255, 255);}
    if (id==2){myColour = color(255, 0, 0);}
    if (id==3){myColour = color(255, 255, 255);}
    if (id==4){myColour = color(0, 0, 255);}
    if (id==5){myColour = color(255, 255, 0);}
    if (id==6){myColour = color(255, 0, 255);}
    
    posX = buttonBlanks + (buttonSize * id) + (buttonSize/2);
    posY = height-(buttonSize/2);
    if (activeCollection==id){
      active=true;
    }
  }
  
  void drawCollButton(){
    
    if (id ==activeCollection){
      active=true;
    } else {
      active=false;
    }
    
    noFill();
    strokeWeight(1);
    stroke(myColour); 
    if (active==true){
      fill(myColour);
    }
      
    ellipse(posX, posY, iconSize, iconSize);
    
    //noFill();
    //stroke(255);
    //ellipse(width/2, height/2, 100, 100);
    //rectMode(CENTER);
    //rect(width/ 2, height/2, width, height);
    
  }
}

void initializeCollectionButtons(){
  
  for (int i=0;i<collections; i++){
    cbObj = new collButtonObject(i);
    cbObjs.add(cbObj);
    collbuttonTotal++;
  }
  
}
