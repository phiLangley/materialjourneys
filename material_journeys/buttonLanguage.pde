langButtonObject lbObj;
ArrayList<langButtonObject> lbObjs;

int languageButtons=1;

class langButtonObject {
  int id;
  
  float posX;
  float posY;
   
  langButtonObject(int index){
    id = index;

    posX = width/2;
    posY = buttonSize/2;

  }
  
  void drawLangButton(){
    
    fill(255);
    
    if (french == false){
      noFill();
    } else {
      fill(255);
    }
    
    strokeWeight(1); 
    stroke(255);
    rectMode(CENTER);
    rect(posX, posY, iconSize, iconSize);
    
    textSize(iconSize/2);
    fill(255,255,255);
    if (french==true){
      fill(0,0,0);
      text("Fr", posX, posY);
    } else {
      fill(255,255,255);
      text("En", posX, posY);
    }
    
    //noFill();
    //stroke(255);
    //ellipse(width/2, height/2, 100, 100);
    //rectMode(CENTER);
    //rect(width/ 2, height/2, width, height);
    
  }
}

void initializeLanguageButtons(){
  
  for (int i=0;i<languageButtons; i++){
    lbObj = new langButtonObject(i);
    lbObjs.add(lbObj);
  }
  
}
