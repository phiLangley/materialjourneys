int boneStacks = 3;
int bonesOffset = 1000;
float boneCount=roomZ;

boneObject bObj;
ArrayList<boneObject> bObjs;

class boneObject{
  
  int id;
  
  float l = random(400,800);
  float w = l;
  float h = random(-roomZ, roomZ/2);
    
  float x = random((-roomX/2)+bonesOffset,(roomX/2)-bonesOffset);
  float y = random((-roomY/2)+bonesOffset,0);
  float z = roomZ;
  
  boneObject(int identity){
    id = identity;
  }
  
  void drawBoneStack(){
    
    strokeWeight(10);
    stroke(255,0,255);
       
    //////draw base
    line(x-l, z, y-w, x+l, z, y-w);
    line(x-l, z, y-w, x-l, z, y+w);
    line(x+l, z, y+w, x-l, z, y+w);
    line(x+l, z, y+w, x+l, z, y-w);
    
    //////draw edges
    if (boneCount>h){
      line(x-l, z, y-w, x, boneCount, y);
      line(x-l, z, y+w, x, boneCount, y);
      line(x+l, z, y-w, x, boneCount, y);
      line(x+l, z, y+w, x, boneCount, y);    
    } else {
      line(x-l, z, y-w, x, h, y);
      line(x-l, z, y+w, x, h, y);
      line(x+l, z, y-w, x, h, y);
      line(x+l, z, y+w, x, h, y);  
    }
  }
  
}

void drawBones(){
  
  boneCount-=2;
  
  for (int i = 0; i<boneStacks; i++){
    boneObject b = bObjs.get(i);
    b.drawBoneStack();
  }
  
}

void initializeBones(){
  
  for (int i = 0; i<boneStacks; i++){
    bObj = new boneObject(i);
    bObjs.add(bObj);
  }
}

void playBones(){
  try {

    fd = context.getAssets().openFd("bones_en.wav");
    if (french==true){
      fd = context.getAssets().openFd("bones_fr.wav");
    }
    snd.setDataSource(fd.getFileDescriptor(),fd.getStartOffset(), fd.getLength());
    snd.prepare();
    snd.start();
    }
    
    catch (IllegalArgumentException e) {
    e.printStackTrace();
    }
    catch (IllegalStateException e) {
    e.printStackTrace();
    } 
    catch (IOException e) {
    e.printStackTrace();
    }
    playBonesAudio=false;
}
